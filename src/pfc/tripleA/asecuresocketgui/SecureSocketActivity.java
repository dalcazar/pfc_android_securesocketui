package pfc.tripleA.asecuresocketgui;
import java.io.UnsupportedEncodingException;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Xml.Encoding;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class SecureSocketActivity extends Activity {	
	private TabSpec tspec;
    private TabHost tabHost;
    private Button buttonConectar;
    private Button buttonDesconectar;
    private EditText editTextIP;
    private EditText editTextPuerto;
    private View viewStatusIcon;
    private TextView textViewStatusMessage;
    
    private TableLayout tableLayoutConversacion;
    private EditText editTextMensaje;
    private Button buttonEnviar;
    private ScrollView scrollViewConversacion;
    
    private CheckBox checkBoxEncriptarPreference;
    private String ENCRIPT_MESSAGES = "encript_messages";
	private boolean encryptMessages = false;
	
	private AndroidSocket androidSocket = new AndroidSocket();
    
    private boolean esIpCorrecto;
    private boolean esPuertoCorrecto;
    
    private enum ConnectionStatus {
    	NULL, DISCONNECTED, DISCONNECTING, BEGIN_CONNECT, CONNECTED
    }
    private ConnectionStatus connectionStatus = ConnectionStatus.DISCONNECTED;
    
    private static final String PREFS_NAME = "MisPreferencias";    

	private static final int MY_NOTIFICATION_ID=1;
	private NotificationManager notificationManager;
	private Notification myNotification;
    
    private enum ApplicationStatus {
    	RUNNING, PAUSE, DISCONNECTING, BEGIN_CONNECT, CONNECTED
    }	
	private ApplicationStatus applicationStatus;
    
    public static StringBuffer toAppend = new StringBuffer("");
    public static StringBuffer toSend = new StringBuffer("");
    	
	public void SetApplicationStatus(int colorResId, int messageResId, ConnectionStatus status) {
		viewStatusIcon.setBackgroundResource(colorResId);
		textViewStatusMessage.setText(messageResId);
		connectionStatus = status;
		UpdateButtonEnableState();
	}
	
	public void UpdateButtonEnableState() {
		switch (connectionStatus) {
		case DISCONNECTED:
			if (esIpCorrecto && esPuertoCorrecto) {
				buttonConectar.setEnabled(true);
			} else {
				buttonConectar.setEnabled(false);
			}
			buttonDesconectar.setEnabled(false);
			break;
			
		case DISCONNECTING:
			buttonConectar.setEnabled(false);
			buttonDesconectar.setEnabled(false);
			break;

		case BEGIN_CONNECT:
			buttonConectar.setEnabled(false);
			buttonDesconectar.setEnabled(false);
			break;

		case CONNECTED:
			buttonConectar.setEnabled(false);
			buttonDesconectar.setEnabled(true);
			break;

		default:
			break;
		}
		if (esIpCorrecto && esPuertoCorrecto && (connectionStatus == ConnectionStatus.DISCONNECTED)){
			buttonConectar.setEnabled(true);
			buttonDesconectar.setEnabled(false);
		}
	}
	
	public class ConnectSocket extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			return androidSocket.InitializeAndroidSocket(editTextIP.getText().toString(), Integer.valueOf(editTextPuerto.getText().toString()));
			
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				SetApplicationStatus(R.color.colorVerde, R.string.conectadoStatusMessageText, ConnectionStatus.CONNECTED);
				new ManageConversation().execute();
			}
			else {
				SetApplicationStatus(R.color.colorRojo, R.string.errorConectandoStatusMessageText, ConnectionStatus.DISCONNECTED);
			}
	    }
	}
    
	public class DisconnectSocket extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			androidSocket.Disconnect();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			SetApplicationStatus(R.color.colorRojo, R.string.desconectadoStatusMessageText, ConnectionStatus.DISCONNECTED);
	    }
	}
	
	public class ManageConversation extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected void onPreExecute(){
			buttonEnviar.setEnabled(true);
		}
		
		@Override
		protected Void doInBackground(Void... params) {			
			while ((connectionStatus == ConnectionStatus.CONNECTED)) {
				try { // Poll every ~10 ms
					Thread.sleep(10);
				} catch (InterruptedException e) { }

				if (toSend.length() != 0) {
					try {
						androidSocket.SendData(encryptMessages ? AES.Encrypt(toSend.toString().getBytes(Encoding.UTF_8.name())) : toSend.toString().getBytes(Encoding.UTF_8.name()));
					} catch (UnsupportedEncodingException e) {
						Log.i("AES", "Error en el encoding");
					}
					toSend.setLength(0);
					publishProgress();
				}

				if (androidSocket.IsDataAvailable()) {
					byte[] dataToRead = androidSocket.ReadData();
					if (dataToRead != null) {
						try {
							appendToChatBox("INCOMING: " + (encryptMessages ? new String(AES.Decrypt(dataToRead), Encoding.UTF_8.name()) : dataToRead.toString()) + "\n");
						} catch (UnsupportedEncodingException e) {
							Log.i("AES", "Error en el encoding");
						}
						publishProgress();
					}
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			buttonEnviar.setEnabled(false);
	    }
		
		@Override
		protected void onProgressUpdate(Void... progress) {
			AddRow(toAppend.toString());
			scrollViewConversacion.postDelayed(new Runnable() {
				
				public void run() {
					scrollViewConversacion.fullScroll(ScrollView.FOCUS_DOWN);
				}
			}, 20);
			
			if (applicationStatus == ApplicationStatus.PAUSE)
			{
				ShowNotification(toAppend);
			}
			toAppend.setLength(0);
	    }
	}
	
	private void AddRow(String text){         
        TableRow row = new TableRow(this);
        TextView t = new TextView(this);
        
        t.setText(text);
        
        if (text.startsWith("OUTGOING: ")) {
        	t.setGravity(Gravity.RIGHT);
        	row.addView(t);
		} else {
			t.setGravity(Gravity.LEFT);
			row.addView(t);
		}
        
        tableLayoutConversacion.addView(row,new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}
	
	public void ShowToastNotification(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

	public void ShowNotification(StringBuffer toAppendText) {
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		myNotification = new Notification(R.drawable.ic_launcher, toAppendText.toString(), System.currentTimeMillis());
		
		Context context = getApplicationContext();
		String notificationTitle = "Conversacion Socket";
	    String notificationText = "Hay nuevos mensajes";
	    Intent myIntent = new Intent(this, SecureSocketActivity.class);
	    myIntent.setAction("android.intent.action.MAIN");
	    myIntent.addCategory("android.intent.category.LAUNCHER");
	    PendingIntent pendingIntent = PendingIntent.getActivity(SecureSocketActivity.this, 0, myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
	    myNotification.defaults |= Notification.DEFAULT_SOUND;
	    myNotification.flags |= Notification.FLAG_AUTO_CANCEL;
	    myNotification.setLatestEventInfo(context, notificationTitle, notificationText, pendingIntent);
	    notificationManager.notify(MY_NOTIFICATION_ID, myNotification);		
	}
	
	private static void appendToChatBox(String s) {
		synchronized (toAppend) {
			toAppend.append(s);
		}
	}

	private static void sendString(String s) {
		synchronized (toSend) {
			toSend.append(s + "\n");
		}
	}
	
	private TextWatcher textWatcher = new TextWatcher() {
		
		public void onTextChanged(CharSequence s, int start, int before, int count) { }
		
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) { }
		
		public void afterTextChanged(Editable s) { 
			UpdateButtonEnableState();
			//Log.i("ASocketGUIlog", "toggleleando el boton!!!; esIpCorrecto = " + esIpCorrecto + "; esPuertoCorrecto = " + esPuertoCorrecto);
		}
	};
 
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {        
    	super.onCreate(savedInstanceState);

    	applicationStatus = ApplicationStatus.RUNNING;
    	
        setContentView(R.layout.activity_secure_socket);
 
        tabHost=(TabHost)findViewById(android.R.id.tabhost);        
        tabHost.setup();
        
        Resources res = getResources() ;
         
        tspec=tabHost.newTabSpec("Tab1");     
        tspec.setContent(R.id.tabConfiguracion);
        tspec.setIndicator(res.getText(R.string.tabConfiguracion)); 
        tabHost.addTab(tspec);
         
        tspec=tabHost.newTabSpec("Tab2");
        tspec.setContent(R.id.tabConversacion);
        tspec.setIndicator(res.getText(R.string.tabConversacion));
        tabHost.addTab(tspec);
        
        tspec=tabHost.newTabSpec("Tab3");     
        tspec.setContent(R.id.tabPreferences);
        tspec.setIndicator(res.getText(R.string.tabPreferences)); 
        tabHost.addTab(tspec);
        
        checkBoxEncriptarPreference = (CheckBox)findViewById(R.id.checkBoxPreferences);
        
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        encryptMessages = settings.getBoolean(ENCRIPT_MESSAGES, false);
        checkBoxEncriptarPreference.setChecked(encryptMessages);
        checkBoxEncriptarPreference.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				encryptMessages = buttonView.isChecked();
				ShowToastNotification(encryptMessages ? R.string.encriptarPreferenceActivado: R.string.encriptarPreferenceDesactivado);
			}
			
        });
        
        //Log.i("ASocketGUIlog", "Lanzando la aplicacion.");
        
        viewStatusIcon = (View)findViewById(R.id.viewStatusIcon);
        
        textViewStatusMessage = (TextView)findViewById(R.id.textViewStatusMessage);
        textViewStatusMessage.setText(R.string.desconectadoStatusMessageText);
        
        buttonConectar = (Button) findViewById(R.id.buttonConectar);
        buttonDesconectar = (Button) findViewById(R.id.buttonDesconectar);

        buttonConectar.setEnabled(false);
        buttonDesconectar.setEnabled(false);
        
        buttonConectar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		        
		        if ( conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED 
		            ||  conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTING  ) {
		        	SetApplicationStatus(R.color.colorNaranja, R.string.conectandoStatusMessageText, ConnectionStatus.BEGIN_CONNECT);
		        	new ConnectSocket().execute();
		        }
		        else if ( conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED 
		            ||  conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {
		        	ShowToastNotification(R.string.noHayConexionInternetText);
		        }
			}
		});
        
        buttonDesconectar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				SetApplicationStatus(R.color.colorNaranja, R.string.desconectandoStatusMessageText, ConnectionStatus.DISCONNECTING);
				new DisconnectSocket().execute();
			}
		});
        
        editTextIP = (EditText)findViewById(R.id.editTextIP);
        editTextPuerto = (EditText)findViewById(R.id.editTextPuerto);
        
        InputFilter[] ipFilters = new InputFilter[1];
        ipFilters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            	//Log.i("ASocketGUIlog", "Validando IP. source = " + source + "; start = " + start + "; end = " + end + "; dest = " + dest + "; dstart = " + dstart + "; dend = " + dend);
            	String resultingTxt = "";
            	
                if (end > start) {
                    resultingTxt = dest.toString().substring(0, dstart) + source.subSequence(start, end) + dest.toString().substring(dend);
                    //Log.i("ASocketGUIlog", "Primer if");
                }
                else if ((start == end) && (start == 0) && (dest.length() > 0) && (source == "")){
                	resultingTxt = dest.subSequence(0, dest.length() - 1).toString();
                	//Log.i("ASocketGUIlog", "Segundo if; (source == \"\") = " + (source == "") + "; (source == null) = " + (source == null));
                }
                else if (!(source == "")){
                	return null;
                	//Log.i("ASocketGUIlog", "Ultimo if; source = " + source + "; dest.lenght = " + dest.length() + "; (source == \"\") = " + (source == "") + "; (source == null) = " + (source == String.));
                }
                
                if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
                	esIpCorrecto = false;
                    return "";
                } else {
                    String[] splits = resultingTxt.split("\\.");
                    esIpCorrecto = (splits.length == 4);
                    for (int i=0; i<splits.length; i++) {
                        if (Integer.valueOf(splits[i]) > 255) {
                        	esIpCorrecto = false;
                            return "";
                        }
                    }
                }
            return null;
            }
        };
        editTextIP.setFilters(ipFilters);
        
        InputFilter[] portFilters = new InputFilter[1];
        portFilters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            	//Log.i("ASocketGUIlog", "Validando Puerto. source = " + source + "; start = " + start + "; end = " + end + "; dest = " + dest + "; dstart = " + dstart + "; dend = " + dend);
            	String resultString = dest.toString();
            	
            	if ((start == end) && (start == 0) && (dest.length() > 0)) {
            		resultString = resultString.substring(0, resultString.length() - 1); 
				} else {
					resultString = dest.toString() + source;
				}
            	//Log.i("ASocketGUIlog", "resultString = " + resultString);
            	try {
	            	esPuertoCorrecto = ((Integer.valueOf(resultString) > 0) && (Integer.valueOf(resultString) < 65535));
            	} catch (NumberFormatException e) {
            		//Log.i("ASocketGUIlog", "e = " + e);
            		esPuertoCorrecto = false;
				}
            	
            	//Log.i("ASocketGUIlog", "esPuertoCorrecto = " + esPuertoCorrecto);
            	if (!esPuertoCorrecto)
            	{
            		return "";
            	}
            return null;
            }
        };
        editTextPuerto.setFilters(portFilters);
        
        editTextIP.addTextChangedListener(textWatcher);
        editTextPuerto.addTextChangedListener(textWatcher);
        
        editTextMensaje = (EditText)findViewById(R.id.editTextMensaje);
        buttonEnviar = (Button)findViewById(R.id.buttonEnviar);
        
        buttonEnviar.setEnabled(false);
        buttonEnviar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				String string = editTextMensaje.getText().toString();
				if (!string.equals("")) {
					appendToChatBox("OUTGOING: " + string + "\n");
					editTextMensaje.setText("");
					
					sendString(string);
				}
			}
		});
        
        scrollViewConversacion = (ScrollView)findViewById(R.id.scrollViewConversacion);
        
        tableLayoutConversacion = (TableLayout) findViewById(R.id.tableLayoutConversacion);
        
        SetApplicationStatus(R.color.colorRojo, R.string.desconectadoStatusMessageText, ConnectionStatus.DISCONNECTED);  
    }
        
    @Override
    protected void onPause() {
    	super.onPause();
    	applicationStatus = ApplicationStatus.PAUSE;
    	//Log.i("appstatus", "en pausa");
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	applicationStatus = ApplicationStatus.RUNNING;
    	//Log.i("appstatus", "running");
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	//Log.i("appstatus", "stopped");
    	SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(ENCRIPT_MESSAGES, checkBoxEncriptarPreference.isChecked());

        // Commit the edits!
        editor.commit();
    }
    
    @Override
    protected void onDestroy() {
    	androidSocket.Disconnect(); //Nos aseguramos de que se cierra correctamente los sockets
    	super.onDestroy();
    }

}