package pfc.tripleA.asecuresocketgui;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

public class AndroidSocket {
	
    private static final int SOCKET_BLOCK_SIZE = 64;
	
    private Socket socket = null;
    private BufferedInputStream in = null;
    private OutputStream out = null;
        
    public boolean InitializeAndroidSocket(String ip, int port) { // Inicializamos el socket
    	boolean noError;
    	try {
			socket = new Socket(ip, port);
			
			in = new BufferedInputStream(socket.getInputStream());
			out = socket.getOutputStream();
			
			noError = true;
		}
		catch (Exception e) {
			CleanUp();
			noError = false;
		}
    	return noError;
    }
    
    public void Disconnect() {
    	CleanUp();
    }
    
    public boolean SendData(byte[] data) { // Metodo para mandar datos
    	boolean noError;
    	
    	try {
    		int remainder = data.length % SOCKET_BLOCK_SIZE; // Miramos si es multiplo de 64
    		if (remainder != 0) {
				data = Arrays.copyOf(data, data.length + SOCKET_BLOCK_SIZE - remainder); // Si no es multiplo de 64, rellenamos hasta que lo sea con 0's
    		}
    		
        	int nBlocks = data.length / SOCKET_BLOCK_SIZE; //Miramos cuantos bloques vamos a tener que mandar
    		for (int i = 0; i < nBlocks; i++) {
    			out.write(Arrays.copyOfRange(data, i * SOCKET_BLOCK_SIZE, i * SOCKET_BLOCK_SIZE + SOCKET_BLOCK_SIZE)); // Mandamos bloque a bloque
    			out.flush();
    		}
			noError = true;
		} catch (IOException e) {
			noError = false;
		}
    	return noError;
    }
    
    public boolean IsDataAvailable() { // Miramos si hay datos disponibles para leer
    	boolean noError = false;
    	boolean isDataAvailable = false;
    	
    	try {
			isDataAvailable = (in.available() != 0); // .available() nos devuelve el numero de bits disponibles para leer
			noError = true;
		} catch (IOException e) {
			noError = false;
		}
    	return (noError && isDataAvailable);
    }
    
    public byte[] ReadData() { //Metodo para leer datos
    	byte[] dataToRead = new byte[SOCKET_BLOCK_SIZE];
		try {
			in.read(dataToRead);
		} catch (IOException e) {
			dataToRead = null;
		}
		return dataToRead;
    }
    
    private void CleanUp() {
        try {
           if (socket != null) {
               socket.close();
               socket = null;
           }
        }
        catch (IOException e) { socket = null; }

        try {
           if (in != null) {
               in.close();
               in = null;
           }
        }
        catch (IOException e) { in = null; }

        try {
            if (out != null) {
            	out.close();
            	out = null;
            }
         }
         catch (IOException e) { out = null; }         
     }
}
