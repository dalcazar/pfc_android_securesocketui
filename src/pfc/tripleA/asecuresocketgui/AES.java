package pfc.tripleA.asecuresocketgui;

import java.security.Key;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

public class AES {
	
	private static final String ALGO = "AES/ECB/NoPadding";
	private static final byte[] keyValue = new byte[] { 'P', 'F', 'C', 't', 
		'r', 'i', 'p', 'l', 'e', 'A', 'D', 'a', 'v', 'i', 'd', '.' };
	private static final Key key = GenerateKey(); // Inicializamos la clave
	
	private static final int AES_BLOCK_SIZE = 16;

	private static Key GenerateKey() {
		Key key = new SecretKeySpec(keyValue, "AES");
		return key;
	}
	
	public static byte[] Encrypt(byte[] data) {
		return transformData(data, Cipher.ENCRYPT_MODE);
	}

	public static byte[] Decrypt(byte[] encryptedData) {
		return transformData(encryptedData, Cipher.DECRYPT_MODE);
	}
	
	private static byte[] transformData(byte[] data, int mode) {
		boolean isValid = false;
		
		int remainder = data.length % AES_BLOCK_SIZE; //Miramos si el bloque es multiplo de 16
		if (remainder != 0) {
			data = Arrays.copyOf(data, data.length + AES_BLOCK_SIZE - remainder); //Rellenamos hasta que sea multiplo de 16, con 0's al final
		}
		
		int nBlocks = data.length / AES_BLOCK_SIZE; // Miramos cuantos bloques tenemos
		byte[] transformedData = new byte[nBlocks * AES_BLOCK_SIZE]; // Variable donde vamos a guardar el resultado
		
		for (int i = 0; i < nBlocks; i++) {
			byte[] auxData = Arrays.copyOfRange(data, i * AES_BLOCK_SIZE, i * AES_BLOCK_SIZE + AES_BLOCK_SIZE); //Nos copiamos el bloque de datos correspondiente
			isValid = false;
			for (int j = 0; j < auxData.length; j++) { //Miramos si es valido (sera valido si no tiene todo 0). Esto lo hacemos para no descifrarlo, porque no tiene sentido ya que se ha metido de relleno
				if (auxData[j] != (byte)0) {
					isValid = true;
				}
			}
			if (isValid) {
				try {
					Cipher c = Cipher.getInstance(ALGO);
					c.init(mode, key);
					byte[] auxTransformedData = c.doFinal(auxData);
					for (int j = 0; j < AES_BLOCK_SIZE; j++) {
						Arrays.fill(transformedData, i * AES_BLOCK_SIZE + j, i * AES_BLOCK_SIZE + j + 1, auxTransformedData[j]); // Vamos copiando byte a byte en el array del resultado final
					}
				} catch (Exception e) {
					transformedData = null;
					Log.i("AES", (mode == Cipher.ENCRYPT_MODE) ? "Error cifrando" : "Error descifrando");
				}
			}
			
		}
		return transformedData;
	}
}
